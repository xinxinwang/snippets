from django.test import TestCase
from django.contrib.auth.models import User
from snippets_app.models import Snippet
from snippets_app.serializers import SnippetSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.compat import BytesIO


class SnippetTestCase(TestCase):
    def setUp(self):
        owner = User(username="user 1")
        snippet = Snippet(title='Python print', code='print("hello, world1")\n', language='python', owner=owner)
        snippet.save()
        snippet = Snippet(title='Java print', code='print("hello, world2")\n', languag='java', owner=owner)
        snippet.save()

        owner = User(username="user 2")
        snippet = Snippet(title='C++ print', code='print("hello, world3")\n', languag='cpp', owner=owner)
        snippet.save()

        print("SetUp done")

    def test_serializer(self):
        snippet = Snippet.objects.get(pk=1)
        print(snippet.code)

        serializer = SnippetSerializer(snippet)
        print("Serialized snippet:", serializer.data)

        content = JSONRenderer().render(serializer.data)
        print("Serialized JSON:", content)

        self.assertEqual(snippet.language, "Python")

        stream = BytesIO(content)
        data = JSONParser().parse(stream)

        serializer = SnippetSerializer(data=data)
        if serializer.is_valid():
            print("Deserialized:", serializer.object.code)

        serializer = SnippetSerializer(Snippet.objects.all(), many=True)
        print("Serialized snippet list:", serializer.data)

