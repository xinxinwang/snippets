# README #

### What is this repository for? ###

* A sample Django project to show how to use Django Rest Framework to generate Restful API for Django models
* Provides a GUI to test the API:
      * A front end GUI for users to view snippets and users API (JSON and HTTP Rest API)
      * A login GUI for users to log in for creation, update and delete snippets
      * An admin GUI to manage users. 
* The project is based on the online tutorial:
       http://www.django-rest-framework.org/tutorial/1-serialization.
   I added the admin URL to manage the users, created test cases, kept the copies of different ways to handle things, and made some other minor changes

### How do I get set up? ###

* Download a copy to your local machine
          git remote add origin https://xinxinwang@bitbucket.org/xinxinwang/snippets.git
* Open the project in  PyCharm Professional Edition

* Run the projects on Server
* Open http://127.0.0.1:8000/
* To log in, click Log-In or open http://127.0.0.1:8000/api-auth/login/. Two users were already added: 
        admin/admin  (super user)
        xinxin/xinxin
* To manage users, open http://127.0.0.1:8000/admin/ and log in as admin/admin

### Additional Notes ###
* serializers_xxx, views_xxx, and urls_xxx are not used. They shows alternative ways
* Unit Test doesn't create the snippet table in the test DB. I am not sure what causes it. The other Django project I created works fine.